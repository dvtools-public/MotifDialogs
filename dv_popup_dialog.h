static char * info_xpm[] = {
"40 40 3 1",
" 	c #5370CB",
".	c #EBEBEB",
"+	c #354273",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                  ....                  ",
"                  ....+                 ",
"                  ....+                 ",
"                   ++++                 ",
"                                        ",
"                                        ",
"                 .....                  ",
"                  ....+                 ",
"                  ....+                 ",
"                  ....+                 ",
"                  ....+                 ",
"                  ....+                 ",
"                  ....+                 ",
"                  ....+                 ",
"                  ....+                 ",
"                  ....+                 ",
"                  ....+                 ",
"                  ....+                 ",
"                  ....+                 ",
"                 ......                 ",
"                  ++++++                ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        "};

static char * error_xpm[] = {
"40 40 3 1",
" 	c #D55555",
".	c #EBEBEB",
"+	c #5A2424",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                ........                ",
"              ............              ",
"            ................            ",
"           .....++++++++.....           ",
"           ....+++++++++++...           ",
"          ......        +++...          ",
"          .......         +...          ",
"         ...++....         +...         ",
"         ...+++....        +...         ",
"         ...  ++....        ...         ",
"         ...   ++....       ...         ",
"         ...    ++....      ...         ",
"         ...     ++....     ...         ",
"         ...      ++....    ...         ",
"         ...       ++....   ...         ",
"         +...       ++.... ...+         ",
"         +...        ++.......+         ",
"          +...        ++.....+          ",
"          +.....       +.....+          ",
"           +................+           ",
"           +++............+++           ",
"            ++++........++++            ",
"              ++++++++++++              ",
"                ++++++++                ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        "};


static char * warn_xpm[] = {
"40 40 3 1",
" 	c #C4B447",
".	c #EBEBEB",
"+	c #807C34",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                   ..                   ",
"                  ....+                 ",
"                 ......                 ",
"                 ......+                ",
"                 ......+                ",
"                 ......+                ",
"                 ......+                ",
"                 ......+                ",
"                 ......+                ",
"                  ....++                ",
"                  ....+                 ",
"                  ....+                 ",
"                  ....+                 ",
"                  ....+                 ",
"                  ....+                 ",
"                   ..++                 ",
"                    ++                  ",
"                                        ",
"                                        ",
"                   ..                   ",
"                  ....                  ",
"                  ....+                 ",
"                   ..++                 ",
"                    ++                  ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        ",
"                                        "};


typedef struct
{
	Display *xtDisplay;
	Window xtWindow;
	Widget topShell;
	Widget callingWidget;
	char *titleString;
	char *messageString;
	
} InfoDialogStruct;



int *getInfoDialogFrameExtents(Widget widget)
{
	Display *display;
	Window window;
	Atom property;
	Atom actualType;
	int actualFormat;
	unsigned long nItems, bytesAfter;
	unsigned char *propReturn = NULL;
	
/* this array is for the border sizes in pixels */
	static int extents[4] = {0, 0, 0, 0};

/* grab display and window associated with input widget */
	display = XtDisplay(widget);
	window = XtWindow(widget);

/* sets the XAtom we're looking for */
	property = XInternAtom(display, "_NET_FRAME_EXTENTS", True);

/* return nothing and exit function if prop not found */
	if(property == None) { return NULL; }

/* if property was found, fetch values */
	if(XGetWindowProperty(display, window, property, 0, 4, False, XA_CARDINAL,
	&actualType, &actualFormat, &nItems, &bytesAfter, &propReturn) == Success)
	{
		if(actualType == XA_CARDINAL && actualFormat == 32 && nItems == 4)
		{
			long *extentsLong = (long *)propReturn;
			extents[0] = (int)extentsLong[0]; /* left */
			extents[1] = (int)extentsLong[1]; /* right */
			extents[2] = (int)extentsLong[2]; /* top - includes title bar */
			extents[3] = (int)extentsLong[3]; /* bottom */
		}
		
		/* clean up */
		XFree(propReturn);
	}
	
	else /* if we could not fetch window property... */
	{
		fprintf(stdout, "Could not grab _NET_FRAME_EXTENTS property.\n");
		return NULL;
	}

/* send results */
	return extents;
}


void getInfoTextDimensions(Widget textWidget, Dimension *textWidth, Dimension *textHeight)
{
/* get text from XmText widget */
	char *text = XmTextGetString(textWidget);

	if(text == NULL)
	{
		*textWidth = 0;
		*textHeight = 0;
		return;
	}

/* get font list */
	XmFontList fontList;
	XtVaGetValues(textWidget, XmNfontList, &fontList, NULL);

/* convert text to XmString */
	XmString xmString = XmStringCreateLocalized(text);

/* get text frame extents */
	XRectangle extent;
	XmStringExtent(fontList, xmString, &extent.width, &extent.height);

/* set the text dimensions */	
	*textWidth = extent.width;
	*textHeight = extent.height;
	
/* clean up */
	XtFree(text);
	XmStringFree(xmString);
}


/* ensure Dismiss gains focus when window is active */
void activeDialog(Widget widget, XtPointer clientData, XEvent *event, Boolean NONE)
{
	Widget infoCancel = (Widget)clientData;
	XmProcessTraversal(infoCancel, XmTRAVERSE_CURRENT);
}


void runInfoDialog(Widget callingWidget, Widget topShell, XtPointer clientData, XtPointer callData,
int iconType, int centerWin, int winFrame)
{

/* grabs user-supplied data from global struct */
	InfoDialogStruct *infoDialogData = (InfoDialogStruct *)clientData;
	
	/* get calling widget */
	Widget caller = infoDialogData->callingWidget;
	
	/* gets window title and message strings from struct */
	char *winTitle = infoDialogData->titleString;
	char *msgString = infoDialogData->messageString;
	
	int icon = iconType;
	int center = centerWin;
	int frame = winFrame;
	
	
/* initialize a bare dialog shell */
	Widget infoDialogShell = XmCreateDialogShell(callingWidget, "infoDialogShell", NULL, 0);
	XtVaSetValues(infoDialogShell,
		XmNtitle, winTitle,
	NULL);
	
	
/* tell vendorshell what kind of window decorations to apply to the dialog */
	int windowDecorations;
		
	if(frame == 0) /* no border */
	{
		XtVaGetValues(infoDialogShell, XmNmwmDecorations, &windowDecorations, NULL);
			windowDecorations &= 0;
		XtVaSetValues(infoDialogShell, XmNmwmDecorations, windowDecorations, NULL);
	}
	
	else if(frame == 1) /* outer border frame only */
	{
		XtVaGetValues(infoDialogShell, XmNmwmDecorations, &windowDecorations, NULL);
			windowDecorations &= ~MWM_DECOR_BORDER;
		XtVaSetValues(infoDialogShell, XmNmwmDecorations, windowDecorations, NULL);
	}
	
	else if(frame == 2) /* title + outer border */
	{
		XtVaGetValues(infoDialogShell, XmNmwmDecorations, &windowDecorations, NULL);
			windowDecorations &= ~MWM_DECOR_TITLE;
			windowDecorations &= ~MWM_DECOR_BORDER;
		XtVaSetValues(infoDialogShell, XmNmwmDecorations, windowDecorations, NULL);
	}
	
	else if(frame == 3) /* menu + title + outer border */
	{
		XtVaGetValues(infoDialogShell, XmNmwmDecorations, &windowDecorations, NULL);
			windowDecorations &= ~MWM_DECOR_MENU;
			windowDecorations &= ~MWM_DECOR_TITLE;
			windowDecorations &= ~MWM_DECOR_BORDER;
		XtVaSetValues(infoDialogShell, XmNmwmDecorations, windowDecorations, NULL);
	}
	
/* create a form to hold the other widgets */
	Widget infoDialogForm = XtVaCreateManagedWidget("infoDialogForm", xmFormWidgetClass, infoDialogShell,
		XmNshadowType, XmSHADOW_OUT,
		XmNshadowThickness, 1,
	NULL);
	
/* cancels the dialog */
	Widget infoCancel = XtVaCreateManagedWidget("infoCancel", xmPushButtonWidgetClass, infoDialogForm,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 8,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 8,
		
		XmNshadowThickness, 2,
		XmNmarginHeight, 4,
		XmNmarginWidth, 7,
		XmNtraversalOn, 1,
		
		XtVaTypedArg, XmNlabelString, XmRString, "Dismiss", 4,
	NULL);
	
/* etched in frame around main message */
	Widget infoLabelFrame = XtVaCreateManagedWidget("infoLabelFrame", xmFrameWidgetClass, infoDialogForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 8,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 8,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 8,
		XmNbottomAttachment, XmATTACH_WIDGET,
		XmNbottomWidget, infoCancel,
		XmNbottomOffset, 6,
		
		XmNshadowType, XmSHADOW_ETCHED_IN,
		XmNshadowThickness, 2,
	NULL);
	
/* main message label */
	Widget infoTextArea = XtVaCreateManagedWidget("infoTextArea", xmTextWidgetClass, infoLabelFrame,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 20,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 2,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 2,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 2,
		
		XmNmarginHeight, 9,
		XmNmarginWidth, 10,
		
		XmNhighlightThickness, 0,
		XmNshadowThickness, 0,
		
		XmNalignment, XmALIGNMENT_BEGINNING,
		XmNeditMode, XmSINGLE_LINE_EDIT,
		XmNeditable, 0,
		XmNscrollVertical, 0,
		XmNscrollHorizontal, 0,
		XmNcursorPositionVisible, 0,
		XmNautoShowCursorPosition, 0,
		XmNtraversalOn, 0,
	NULL);
	XmTextSetString(infoTextArea, msgString);
	
	
/* get colors from main dialog XmForm */
	Pixel mainBG, mainFG;
	XtVaGetValues(infoDialogForm,
		XmNbackground, &mainBG,
		XmNforeground, &mainFG,
	NULL);
	
	/* apply colors to XmText widget for consistency */
	XtVaSetValues(infoTextArea,
		XmNbackground, mainBG,
		XmNforeground, mainFG,
	NULL);
	
/* get height/width of text and widgets */
	Dimension textWidth, textHeight, buttonHeight;
	getInfoTextDimensions(infoTextArea, &textWidth, &textHeight);
	XtVaGetValues(infoCancel, XmNheight, &buttonHeight, NULL);
	
	
/* create icon pixmaps */
Pixmap info_pixmap, info_mask;
	XpmCreatePixmapFromData(XtDisplay(infoDialogShell), 
	RootWindowOfScreen(XtScreen(infoDialogShell)), 
	info_xpm, &info_pixmap, &info_mask, NULL);

Pixmap error_pixmap, error_mask;
	XpmCreatePixmapFromData(XtDisplay(infoDialogShell), 
	RootWindowOfScreen(XtScreen(infoDialogShell)), 
	error_xpm, &error_pixmap, &error_mask, NULL);
	
Pixmap warn_pixmap, warn_mask;
	XpmCreatePixmapFromData(XtDisplay(infoDialogShell), 
	RootWindowOfScreen(XtScreen(infoDialogShell)), 
	warn_xpm, &warn_pixmap, &warn_mask, NULL);
	
	
	int finalWidth, finalHeight;
	
/* if icon is enabled */
	if(icon != 0)
	{
		finalWidth = textWidth + 90;
		finalHeight = textHeight + 38 + buttonHeight;
		
		/* create icon frame */
		Widget infoIconFrame = XtVaCreateManagedWidget("infoIconFrame", xmFrameWidgetClass, infoDialogForm,
			XmNtopAttachment, XmATTACH_FORM,
			XmNtopOffset, 8,
			XmNleftAttachment, XmATTACH_FORM,
			XmNleftOffset, 8,
			XmNheight, 42,
			XmNwidth, 42,
			
			XmNshadowType, XmSHADOW_IN,
			XmNshadowThickness, 1,
		NULL);
		
		XtVaSetValues(infoLabelFrame, 
			XmNleftAttachment, XmATTACH_WIDGET,
			XmNleftWidget, infoIconFrame,
			XmNleftOffset, 6,
		NULL);
		
/* set icon type based on argument */
		if(icon == 1) { XtVaSetValues(infoIconFrame, XmNbackgroundPixmap, info_pixmap, NULL); }
		
		if(icon == 2) { XtVaSetValues(infoIconFrame, XmNbackgroundPixmap, error_pixmap, NULL); }
		
		if(icon == 3) { XtVaSetValues(infoIconFrame, XmNbackgroundPixmap, warn_pixmap, NULL); }
	}
	
	if(icon == 0)
	{
		finalWidth = textWidth + 38; /* 38px accounts for widget margins and offsets */
		finalHeight = textHeight + 38 + buttonHeight;
	}
	
/* fixup height values so the frame aligns vertically with the icon frame */
	if(textHeight < 26)
	{
		int alignWithIcon = 26 - textHeight;
		finalHeight = textHeight + 38 + buttonHeight + alignWithIcon;
	}
	

/* get _NET_FRAME_EXTENTS for window border and title bar of the dialog shell */
	int borderTop, borderLeft, borderRight, borderBottom;
	int *extents = getInfoDialogFrameExtents(infoDialogShell);
	if(extents != NULL)
	{
		borderLeft = extents[0];
		borderRight = extents[1];
		borderTop = extents[2];
		borderBottom = extents[3];
	}
	
	
/* figure out where top level window is positioned */
	Dimension topShellW, topShellH, topShellX, topShellY;
	XtVaGetValues(topShell,
		XmNwidth, &topShellW,
		XmNheight, &topShellH,
		XmNx, &topShellX,
		XmNy, &topShellY,
	NULL);
	
	
/* figure out where dialog shell window is positioned */
	Dimension infoDialogX, infoDialogY;
	XtVaGetValues(infoDialogForm,
		XmNx, &infoDialogX,
		XmNy, &infoDialogY,
	NULL);
	
/* if window centering is enabled... */
	if(center == 1)
	{
		int newX, newY;
		
		if(frame == 0 || frame == 1)
		{
			newX = topShellX + (topShellW / 2) - (finalWidth / 2) - borderLeft;
			newY = topShellY + (topShellH / 2) - (finalHeight / 2) - borderBottom;
		}
		
		else
		{
			newX = topShellX + (topShellW / 2) - (finalWidth / 2) - borderLeft;
			newY = topShellY + (topShellH / 2) - (finalHeight / 2) - borderTop;
		}
		
		/* set window geometry and position */
		XtVaSetValues(infoDialogShell,
			XmNwidth, finalWidth, 
			XmNheight, finalHeight,
			XmNx, newX,
			XmNy, newY,
		NULL);
	}
	
	else if(center == 0)
	{
		XtVaSetValues(infoDialogShell,
			XmNwidth, finalWidth, 
			XmNheight, finalHeight,
		NULL);
	}
	
	
/* give dismiss button default focus */
	XtAddEventHandler(infoDialogShell, ExposureMask, False, 
	(XtEventHandler)activeDialog, (XtPointer)infoCancel);
}


void createInfoDialog(Widget callingWidget, Widget topShell, 
char *titleString, char *msgString, int iconType, int centerWin, int winFrame)
{
/* pass along incoming data to struct */
	InfoDialogStruct infoDialogData;
	
	infoDialogData.callingWidget = callingWidget;
	infoDialogData.titleString = titleString;
	infoDialogData.messageString = msgString;
	
/* run the dialog */
	runInfoDialog(callingWidget, topShell, (XtPointer)&infoDialogData,
	(XtPointer)&infoDialogData, iconType, centerWin, winFrame);
}