COMPILER = cc
SOURCE = demo.c
CFLAGS = -O3
LDFLAGS = -lXm -lX11 -lXt -lXpm
TARGET = ./demo

.PHONY: all clean install uninstall

all: $(TARGET)

$(TARGET): $(SOURCE)
	$(COMPILER) $(CFLAGS) $^ $(LDFLAGS) -o $@
clean:
	rm -f $(TARGET)
