### Custom Motif Dialogs

I wasn't satisfied with stock Motif dialogs for my program About/Help and other stuff so I made a reusable C header.


![screenshot](screenshots/dvdialogs_sample.png)



#### Primary Features

- Display custom title and message strings.
- Center popup in main window or post under mouse cursor.
- Choice of Info, Error, Warning or no icon.
- Control which MWM window decorations are shown.


#### Secondary Features

The popup can be used in conjunction with other system commands to display their text output. The demo has a 'sensors -f' button to show how this can be done.


#### Requirements

- Motif 2.3.8
- X11/Xlib
- libXpm


#### Build Instructions

Compile:

```
make clean && make
```

Run:

```
./demo
```

Usage:

```
createInfoDialog(Widget, Widget, char, char, int, int, int);
createInfoDialog(calling widget, top shell, "title", "message", 0, 0, 0);


calling widget
	
	The widget that launched the dialog.

top shell
	
	Application's main top level shell widget.

win title
	
	Title string to be applied to popup window.

main message
	
	Message string to be displayed.

icon type
	
	0 = no icon
	1 = info
	2 = error
	3 = warning

win centering

	0 = do not center
	1 = center inside top level shell

win frame

	0 = borderless
	1 = frame only
	2 = title + frame
	3 = menu + title + frame

```



#### License

This software is distributed free of charge under the BSD Zero Clause license.