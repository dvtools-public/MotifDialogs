
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <Xm/Xm.h>
#include <Xm/Form.h>
#include <Xm/PushB.h>
#include <Xm/Label.h>
#include <Xm/Text.h>
#include <Xm/Frame.h>
#include <Xm/Separator.h>
#include <Xm/XmStrDefs.h>
#include <Xm/DialogS.h>
#include <Xm/MwmUtil.h>

#include <X11/Xlib.h>
#include <X11/Shell.h>
#include <X11/Intrinsic.h>
#include <X11/Xatom.h>
#include <X11/xpm.h>


#include "dv_popup_dialog.h"

/*

Usage:


createInfoDialog(Widget, Widget, char, char, int, int, int);

createInfoDialog(button, topLevel, "title", "message", 0, 0, 0);


calling widget
	
	The widget that launched the dialog.

top shell
	
	Application's main top level shell widget.

win title
	
	Title string to be applied to popup window.

main message
	
	Message string to be displayed.

icon type
	
	0 = no icon
	1 = info
	2 = error
	3 = warning

win centering

	0 = do not center
	1 = center inside top level shell.

win frame

	0 = borderless
	1 = frame only
	2 = title + frame
	3 = menu + title + frame

*/

void textButtonCallback(Widget callingWidget, XtPointer clientData, XtPointer callData)
{
/* make sure topShell (topLevel) widget data gets passed properly - very important for geometry ops */
	Widget topLevel = (Widget)clientData;
	createInfoDialog(callingWidget, topLevel, "None", "This should open with the top left corner over the button.", 0, 0, 0);
}


void infoButtonCallback(Widget callingWidget, XtPointer clientData, XtPointer callData)
{
	Widget topLevel = (Widget)clientData;
	createInfoDialog(callingWidget, topLevel, "Info", "Your computer is running. Better go catch it!", 1, 1, 1);
}


void errorButtonCallback(Widget callingWidget, XtPointer clientData, XtPointer callData)
{
	Widget topLevel = (Widget)clientData;
	createInfoDialog(callingWidget, topLevel, "Error", "ILLEGAL OPERATION. THE WHOLE DATACENTER IS ON FIRE.", 2, 1, 2);
}


void warnButtonCallback(Widget callingWidget, XtPointer clientData, XtPointer callData)
{
	Widget topLevel = (Widget)clientData;
	createInfoDialog(callingWidget, topLevel, "Warning", "The end of the world is near!", 3, 1, 3);
}


void tempButtonCallback(Widget callingWidget, XtPointer clientData, XtPointer callData)
{
	Widget topLevel = (Widget)clientData;
	
	FILE *cmdPipe;
	char cmdOutputBuffer[4096];

/* run lm-sensors command to get core temperatures */
/*
	Piping into sed here was necessary to add 6 extra spaces
	as padding. XmText widget does not have perfect handling
	for every Unicode character. It will occasionally choke
	on random ones such as the degree symbol (U+00B0).
*/
	cmdPipe = popen("sensors -f | sed 's/$/      /'", "r");
	fread(cmdOutputBuffer, 1, sizeof(cmdOutputBuffer) - 1, cmdPipe);
	pclose(cmdPipe);
	

	createInfoDialog(callingWidget, topLevel, "Core Temps", cmdOutputBuffer, 0, 1, 3);
}




int main(int argc, char *argv[])
{
/* define xt context */
	XtAppContext app;
	
/* open top level window shell */
	Widget topLevel = XtVaAppInitialize(&app, "demo", NULL, 0, &argc, argv, NULL, NULL);
	XtVaSetValues(topLevel,
		XmNtitle, "Custom Motif Dialogs",
		XmNwidth, 640,
		XmNheight, 480,
		XmNx, 120,
		XmNy, 120,
	NULL);

	
/* create form as manager widget */
	Widget mainForm = XtVaCreateManagedWidget("mainForm", xmFormWidgetClass, topLevel,
		XmNshadowThickness, 0,
	NULL);
	

/* plain text dialog */
	Widget textButton = XtVaCreateManagedWidget("textButton", xmPushButtonWidgetClass, mainForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 20,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 20,
		
		XmNshadowThickness, 2,
		XmNmarginHeight, 4,
		XmNmarginWidth, 7,
		
		XtVaTypedArg, XmNlabelString, XmRString, "Plain", 4,
	NULL);
	XtAddCallback(textButton, XmNactivateCallback, textButtonCallback, (XtPointer)topLevel);
	

/* info dialog */
	Widget infoButton = XtVaCreateManagedWidget("infoButton", xmPushButtonWidgetClass, mainForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 20,
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, textButton,
		XmNleftOffset, 10,
		
		XmNshadowThickness, 2,
		XmNmarginHeight, 4,
		XmNmarginWidth, 7,
		
		XtVaTypedArg, XmNlabelString, XmRString, "Info Dialog", 4,
	NULL);
	XtAddCallback(infoButton, XmNactivateCallback, infoButtonCallback, (XtPointer)topLevel);
	
	
/* error dialog */
	Widget errorButton = XtVaCreateManagedWidget("errorButton", xmPushButtonWidgetClass, mainForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 20,
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, infoButton,
		XmNleftOffset, 10,
		
		XmNshadowThickness, 2,
		XmNmarginHeight, 4,
		XmNmarginWidth, 7,
		
		XtVaTypedArg, XmNlabelString, XmRString, "Error Dialog", 4,
	NULL);
	XtAddCallback(errorButton, XmNactivateCallback, errorButtonCallback, (XtPointer)topLevel);
	
	
/* warning dialog */
	Widget warnButton = XtVaCreateManagedWidget("warnButton", xmPushButtonWidgetClass, mainForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 20,
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, errorButton,
		XmNleftOffset, 10,
		
		XmNshadowThickness, 2,
		XmNmarginHeight, 4,
		XmNmarginWidth, 7,
		
		XtVaTypedArg, XmNlabelString, XmRString, "Warning Dialog", 4,
	NULL);
	XtAddCallback(warnButton, XmNactivateCallback, warnButtonCallback, (XtPointer)topLevel);


/* temperature dialog */
	Widget tempButton = XtVaCreateManagedWidget("tempButton", xmPushButtonWidgetClass, mainForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 20,
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, warnButton,
		XmNleftOffset, 10,
		
		XmNshadowThickness, 2,
		XmNmarginHeight, 4,
		XmNmarginWidth, 7,
		
		XtVaTypedArg, XmNlabelString, XmRString, "sensors -f", 4,
	NULL);
	XtAddCallback(tempButton, XmNactivateCallback, tempButtonCallback, (XtPointer)topLevel);

	
/* create separator */
	Widget sep0 = XtVaCreateManagedWidget("sep0", xmSeparatorWidgetClass, mainForm,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, textButton,
		XmNtopOffset, 20,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 16,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 16,
		
		XmNseparatorType, XmSINGLE_LINE,
	NULL);
	
	
	
/* close top level */
	XtRealizeWidget(topLevel);
	
	
/* creates centered info dialog on startup */

	/* createInfoDialog(calling widget, top shell, win title, main message, icon type, win centering, win frame); */
	createInfoDialog(topLevel, topLevel, "Startup Window", "This opens with the main window.", 1, 1, 3);
	
	
/* enter processing loop */
	XtAppMainLoop(app);

	return 0;
}
